import styled, { keyframes } from 'styled-components'
import sizes from '../../../styles/sizes'
import colors from '../../../styles/colors'

export const MainWrapper = styled.div`
  max-width: 100%;
  min-width: 210px;
  margin: ${props => props.marginRightBottom && '0 10px 15px 0'};
  flex: 1;
  display: flex;
  outline: none;
  position: relative;
  flex-direction: column;
  height: ${props => (props.fullHeight ? '100%' : '46px')};
`

const openAnimation = keyframes`
  from {
    max-height: 0;
  }
  to {
    max-height: 320px; 
  }
`

const closeAnimation = keyframes`
from {
  max-height: 320px;
}
to {
  max-height: 0; 
}
`

const openHandler = ({ isDropdownOpen, height }) => {
  if (!isDropdownOpen && height) {
    return `${closeAnimation} 0.1s linear forwards`
  }
  if (isDropdownOpen && height) {
    return `${openAnimation} 0.1s linear forwards`
  }
  return null
}

export const Drop = styled.div`
  box-sizing: border-box;
  position: absolute;
  border-radius: ${sizes.borderRadius};
  width: 100%;
  z-index: ${sizes.dropdownIndex};
  top: ${sizes.inputFieldHeight};
  flex-direction: column;
  align-items: space-between;
  overflow-y: auto;
  ::-webkit-scrollbar {
    width: 5px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 5px;
    background-color: lightgray;
    outline: 1px solid slategrey;
  }
  background-color: white;
  animation: ${openHandler};
  max-height: 0;
  box-shadow: ${colors.primaryBoxShadow};
`

export const Wrapper = styled.div`
  box-sizing: border-box;
  min-width: 210px;
  color: ${colors.primaryText};
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-right: 20px;
  height: ${sizes.inputFieldHeight};
  border-radius: ${sizes.borderRadius};
  background-color: white;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.2);
  flex: 1;
`

export const InnerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`
export const SelectedWrapper = styled.div`
  display: flex;
  padding: 15px;
  align-items: center;
`

export const DropItem = styled.div`
  &:hover {
    background-color: lightGray;
  }
  padding: 15px;
  display: flex;
  align-items: center;
  cursor: pointer;
`

export const Text = styled.div`
  padding-left: 10px;
`

const openArrowRotation = keyframes`
  from {
  transform: rotate(0deg);

  }
  to {
  transform: rotate(180deg);

  }
`

const closeArrowRotation = keyframes`
  from {
  transform: rotate(180deg);

  }
  to {
  transform: rotate(0deg);

  }
  `

const rotationHandler = ({ isDropdownOpen, height }) => {
  if (!isDropdownOpen && height) {
    return `${closeArrowRotation} 0.2s linear forwards`
  }

  if (isDropdownOpen && height) {
    return `${openArrowRotation} 0.2s linear forwards`
  }
  return null
}

export const Arrow = styled.div`
  transform: rotate(0deg);
  animation: ${rotationHandler};
`

export const Text2 = styled.div`
  background-color: white;
  position: absolute;
  color: ${colors.primaryText};
  left: 20px;
  padding: 0 4px;
  top: -8px;
  font-size: 11px;
`
