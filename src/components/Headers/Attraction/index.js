import React, { Component } from 'react'
import {
  //  PlacesAutocomplete,
  LogoDropdown,
  CustomDropdown,
  AutcompleteInput,
  DatePicker,
  PersonsDropdown
} from '../..'
import { MainWrapper, ThirdRow, InnerWrapper } from './styles'
import {
  FilterIcon,
  EditIcon,
  AirplaneIcon,
  HotelIcon,
  RestaurantIcon,
  TransportIcon,
  ActivityIcon,
  SightIcon
} from '../../../assets/icons'
import kayak from '../../../assets/logos/kayak.png'
import pegasus from '../../../assets/logos/pegasus.png'
import skyscanner from '../../../assets/logos/skyscanner.png'
import expedia from '../../../assets/logos/expedia.png'
import priceline from '../../../assets/logos/priceline.png'
import booking from '../../../assets/logos/booking.png'
import tripadvisor from '../../../assets/logos/tripadvisor.png'
import makemytrip from '../../../assets/logos/makemytrip.png'
import googleFlights from '../../../assets/logos/google-flights.png'

class FlightsHeader extends Component {
  state = {}

  render() {
    const { defaultSelected, changeHeader } = this.props

    return (
      <MainWrapper>
        {window.innerWidth > 768 && (
          <CustomDropdown
            defaultSelected={defaultSelected}
            onItemSelect={changeHeader}
            marginRightBottom
            title="Category"
            width={20}
            height={20}
            icons={[
              AirplaneIcon,
              HotelIcon,
              SightIcon,
              ActivityIcon,
              RestaurantIcon,
              TransportIcon
            ]}
            values={[
              'Flights',
              'Hotels',
              'Sightseeings',
              'Activities',
              'Restaurants',
              'Transports'
            ]}
          />
        )}
        <AutcompleteInput
          title="Where"
          placeholder="Enter city or airport"
          noMinWidth
          defaultValue="London, UK"
          marginRightBottom
          id="autocomplete2"
        />
        {/* <PlacesAutocomplete placeholder="Where" marginRightBottom title="Where" /> */}
        <LogoDropdown
          header="Sightseeings From"
          width={140}
          selected={tripadvisor}
          marginRightBottom
        >
          <img alt="bnb" src={skyscanner} style={{ width: 130 }} />
          <img alt="bnb" src={kayak} style={{ width: 110 }} />
          <img alt="bnb" src={priceline} style={{ width: 110 }} />
          <img alt="bnb" src={pegasus} style={{ width: 130 }} />
          <img alt="bnb" src={tripadvisor} style={{ width: 130 }} />
          <img alt="bnb" src={expedia} style={{ width: 110 }} />
          <img alt="bnb" src={booking} style={{ width: 130 }} />
          <img alt="bnb" src={makemytrip} style={{ width: 110 }} />
        </LogoDropdown>
        <DatePicker />
        <PersonsDropdown marginRightBottom header />
        <ThirdRow>
          <InnerWrapper>
            <EditIcon color="#00b2d6" height={20} width={20} /> Create Custom Sightseeing
          </InnerWrapper>
          <InnerWrapper>
            <FilterIcon color="#00b2d6" height={20} width={20} />
            Advanced
          </InnerWrapper>
        </ThirdRow>
      </MainWrapper>
    )
  }
}

export default FlightsHeader
