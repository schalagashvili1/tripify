import styled from 'styled-components'

export const GroupStyle = styled.div`
  margin-left: 50px;
  flex: 1;
`

export const Wrapper = styled.div`
  display: flex;
  justify-content: stretch;
  margin-top: 50px;
  margin-right: 50px;
`
