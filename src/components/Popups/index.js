import HotelDetailsPopup from './HotelDetails'
import LoginPopup from './Login'
import SignupPopup from './Signup'
import AttractionDetailsPopup from './AttractionsDetails'

export { HotelDetailsPopup, LoginPopup, SignupPopup, AttractionDetailsPopup }
