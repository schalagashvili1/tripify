import React, { Component } from 'react'
import { TweenLite } from 'gsap'
import {
  MainWrapper,
  Overlay,
  Button,
  ContentWrapper,
  Wrapper,
  HotelName,
  HorizontalLine,
  ArrowDownWrapper,
  SectionWrapper,
  FacilitySecondColumn,
  FacilityFirstColumn,
  RoomInfoWrapper
} from './styles'
import { Slider } from '../..'
import TripDetails from '../../../assets/images/tripabout.png'
import { ArrowDownIcon } from '../../../assets/icons'

class DetailsPopup extends Component {
  state = {}

  render() {
    return (
      <MainWrapper id="attractionDetailsPopup">
        <Overlay
          onClick={() =>
            TweenLite.to('#attractionDetailsPopup', 0.4, { opacity: 0, display: 'none' })
          }
        >
          <Button
            onClick={() =>
              TweenLite.to('#attractionDetailsPopup', 0.4, { opacity: 0, display: 'none' })
            }
          >
            Close
          </Button>
          <Wrapper>
            <ContentWrapper onClick={e => e.stopPropagation()}>
              <Slider />
              <img src={TripDetails} style={{ width: 800 }} />
            </ContentWrapper>
          </Wrapper>
        </Overlay>
      </MainWrapper>
    )
  }
}

export default DetailsPopup
