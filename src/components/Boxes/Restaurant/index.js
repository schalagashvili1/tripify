import React, { Component } from 'react'
import {
  MainWrapper,
  Img,
  Name,
  FirstColumn,
  LocationWrapper,
  MoreInfo,
  SecondColumn,
  PriceWrapper,
  Button,
  SectionWrapper,
  RoomsAndPrice,
  Rooms,
  RatingWrapper,
  Tags,
  Price
} from './styles'
import Stars from '../../../assets/images/openstars.png'
import { PinIcon, CloseX } from '../../../assets/icons'

// eslint-disable-next-line react/prefer-stateless-function
class FlightBox extends Component {
  render() {
    const {
      imageURL,
      paddingLeft,
      noBorderRadius,
      left,
      closeButton,
      name,
      reviewsCount,
      people,
      address,
      price
    } = this.props

    return (
      <MainWrapper paddingLeft={paddingLeft}>
        <Img left={left} src={imageURL} noBorderRadius={noBorderRadius} />
        <FirstColumn>
          <Name>{name}</Name>
          <RatingWrapper>
            <img
              src={Stars}
              style={{
                width: 85,
                display: 'flex',
                marginRight: 5
              }}
              alt="stars"
            />
            {reviewsCount} Reviews
          </RatingWrapper>
          <LocationWrapper>
            <PinIcon color="#484848" height={13} width={13} />
            {address}
          </LocationWrapper>
          <Tags>Sushi, Candy, Chinese, Lunch</Tags>
          <MoreInfo>More Details</MoreInfo>
        </FirstColumn>
        <SecondColumn>
          {closeButton && (
            <div style={{ right: 10, top: 15 }}>
              <CloseX color="rgb(51, 51, 51)" height={11} width={11} />
            </div>
          )}
          <Price>
            $$$
            {price}
          </Price>
          <SectionWrapper>
            <RoomsAndPrice>
              <PriceWrapper>${price}</PriceWrapper>
              <Rooms>for 2 persons</Rooms>
            </RoomsAndPrice>
          </SectionWrapper>
          <Button>Book Now</Button>
        </SecondColumn>
      </MainWrapper>
    )
  }
}

export default FlightBox
