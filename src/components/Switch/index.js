import React, { Component } from 'react'
import TweenLite from 'gsap'
import { Switch, CheckBox, Slider } from './styles'

class SwitchSlider extends Component {
  state = {
    enabled: false
  }

  mapHandler = target => {
    const { enabled } = this.state

    if (!enabled) {
      TweenLite.to('#directionsMap2', 0.4, { right: 0 })
    } else {
      TweenLite.to('#directionsMap2', 0.4, { right: '-800px' })
    }
    this.setState({ enabled: target.checked })
  }

  toggleHandler = target => {
    this.setState({ enabled: target.checked })
  }

  render() {
    const { enabled } = this.state
    const { map, optOn } = this.props

    return (
      <Switch>
        {map ? (
          <CheckBox
            checked={enabled}
            onChange={({ target }) => (map ? this.mapHandler(target) : this.toggleHandler(target))}
            type="checkbox"
          />
        ) : (
          <CheckBox
            checked={enabled}
            onChange={({ target }) => {
              this.toggleHandler(target)
              optOn()
            }}
            type="checkbox"
          />
        )}
        <Slider />
      </Switch>
    )
  }
}

export default SwitchSlider
