import React, { Component } from 'react'
import { DragDropContext } from 'react-beautiful-dnd'
import TweenLite from 'gsap'
import ScrollToPlugin from 'gsap/ScrollToPlugin'
import GoogleMaps from '../../components/GoogleMaps'
import {
  closeHandler,
  directionsMapClose,
  directionsMapOpen,
  multipleMarkersMapClose
} from './functions'
import {
  HotelsHeader,
  FlightsHeader,
  SightseeingsHeader,
  TransportsHeader,
  RestaurantsHeader,
  ActivitiesHeader,
  HotelDetailsPopup,
  AttractionDetailsPopup,
  RightFixedHeader,
  SwitchSlider
} from '../../components'
import {
  LeftBar,
  RightBar,
  MainWrapper,
  BoxWrapper,
  RightInsideWrapper,
  Button,
  Overlay,
  LeftBarOverlay,
  BackgroundLayer,
  MultipleMarkersMapWrapper,
  MultipleMarkersMap,
  CostWrapper,
  CostText,
  Cost,
  LeftHeader,
  MapView,
  DirectionsMapWrapper,
  DirectionsMap,
  RoutesByDays,
  Day,
  QuestionMark,
  QuestionInfo,
  InfoWrapper
} from './styles'
import { PinIcon, QuestionIcon } from '../../assets/icons'
import LeftDrop from '../../components/dnd/Left'
import RightDrop from '../../components/dnd/Right'
import externalState from './state'
import date1 from '../../assets/images/date1.png'
import date2 from '../../assets/images/date2.png'

import './styles.css'

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)
  return result
}

const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source)
  const destClone = Array.from(destination)
  const [removed] = sourceClone.splice(droppableSource.index, 1)

  destClone.splice(droppableDestination.index, 0, removed)

  const result = {}
  result[droppableSource.droppableId] = sourceClone
  result[droppableDestination.droppableId] = destClone

  return result
}
class TripPlanner extends Component {
  state = externalState

  componentDidMount() {
    const { lists, flights } = this.state

    lists[0].list = flights
    this.setState({ list1: flights })

    setTimeout(() => {
      this.setState({ autocompleteClicked: true })
    }, 2000)
  }

  getList = id => {
    const { lists } = this.state
    let item
    lists.forEach(listItem => {
      if (listItem.droppableId === id) {
        item = listItem.list
      }
    })
    return item
  }

  onDragEnd = result => {
    const { lists, flight2, hotel1, hotel2 } = this.state

    if (lists[1].list.length === 0) {
      lists[3].list.push(flight2)

      this.setState({ [lists[3]]: lists[1] })
    }

    if (lists[1].list.length === 1) {
      lists[2].list.push(hotel1)
      lists[3].list.unshift(hotel2)

      this.setState({ [lists[3]]: lists[1] })
    }

    const { source, destination } = result

    if (!destination) {
      return
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(this.getList(source.droppableId), source.index, destination.index)
      const copiedState = Object.assign({}, this.state)

      copiedState.lists.forEach(listItem => {
        if (source.droppableId === listItem.droppableId) {
          listItem.list = items
        }
      })

      this.setState(copiedState)
    } else {
      const result = move(
        this.getList(source.droppableId),
        this.getList(destination.droppableId),
        source,
        destination
      )
      if (this.state.totalCosts === 0) {
        this.setState({ totalCosts: this.state.totalCosts + 497 })
      } else {
        this.setState({ totalCosts: this.state.totalCosts + 75 })
      }

      lists.forEach(listi => {
        if (result[listi.droppableId] !== undefined) {
          listi.list = result[listi.droppableId] ? result[listi.droppableId] : listi.list

          this.setState({
            [listi.list]: listi.list ? result[listi.droppableId] : listi.list
          })
        }
      })
    }
  }

  changeCategory = category => {
    const { lists, flights, hotels, sightseeings, restaurants, activities } = this.state

    switch (category) {
      case 'Flights':
        lists[0].list = flights
        return this.setState({ list1: flights, currentHeader: category })
      case 'Hotels':
        lists[0].list = hotels
        return this.setState({ list1: hotels, currentHeader: category })
      case 'Sightseeings':
        lists[0].list = sightseeings
        return this.setState({ list1: sightseeings, currentHeader: category })
      case 'Activities':
        lists[0].list = activities
        return this.setState({ list1: activities, currentHeader: category })
      case 'Restaurants':
        lists[0].list = restaurants
        return this.setState({ list1: restaurants, currentHeader: category })
      case 'Transports':
        return <BoxWrapper />
      default:
        return null
    }
  }

  renderHeader = () => {
    const { currentHeader } = this.state
    const { changeCategory } = this

    switch (currentHeader) {
      case 'Flights':
        return <FlightsHeader changeHeader={changeCategory} defaultSelected="Flights" />
      case 'Hotels':
        return <HotelsHeader changeHeader={changeCategory} defaultSelected="Hotels" />
      case 'Sightseeings':
        return <SightseeingsHeader changeHeader={changeCategory} defaultSelected="Sightseeings" />
      case 'Restaurants':
        return <RestaurantsHeader changeHeader={changeCategory} defaultSelected="Restaurants" />
      case 'Activities':
        return <ActivitiesHeader changeHeader={changeCategory} defaultSelected="Activities" />
      case 'Transports':
        return <TransportsHeader changeHeader={changeCategory} defaultSelected="Transports" />
      default:
        return null
    }
  }

  optimizationOn = () => {
    this.setState({ loading: true })
    setTimeout(() => {
      this.setState({ loading: false })
    }, 800)
  }

  addNewDay = newDay => {
    const { lists } = this.state
    lists.push(newDay)
    this.forceUpdate()
  }

  mapHandler = e => {
    const { isMapOpen } = this.state
    e.preventDefault()

    if (!isMapOpen) {
      TweenLite.to('#directionsMap2', 0.4, { right: 0 })
    } else {
      TweenLite.to('#directionsMap2', 0.4, { right: '-800px' })
    }
    this.setState(prevState => ({
      isMapOpen: !prevState.isMapOpen
    }))
  }

  render() {
    const {
      date,
      autocompleteClicked,
      totalCosts,
      lists,
      loading,
      direction,
      currentHeader
    } = this.state

    return (
      <MainWrapper>
        {loading && (
          <div className="lds-ellipsis">
            <div />
            <div />
            <div />
            <div />
          </div>
        )}
        {autocompleteClicked && !direction && <GoogleMaps day1 />}
        {direction && <GoogleMaps day1={false} />}

        <DragDropContext onDragEnd={this.onDragEnd}>
          <LeftBar onClick={() => this.changeCategory} id="leftBarr">
            {autocompleteClicked && (
              <MultipleMarkersMapWrapper id="multipleMarkersMap2">
                <MultipleMarkersMap id="multipleMarkersMap" />
                <Button
                  right
                  onClick={() => {
                    multipleMarkersMapClose()
                  }}
                >
                  ⭠ Close
                </Button>
              </MultipleMarkersMapWrapper>
            )}
            <BackgroundLayer />
            <LeftBarOverlay id="left-bar-overlay" onClick={closeHandler} />
            <img
              alt="img"
              src={date ? date2 : date1}
              style={{ position: 'fixed', width: 50, left: 10, top: '34%', cursor: 'pointer' }}
              onClick={() => {
                TweenLite.to('#leftBarr', 0.5, {
                  scrollTo: {
                    y: 'max'
                  }
                })
                this.setState({ date: true })
                // setTimeout(() => {
                //   this.setState({ date: false })
                // }, 2800)
              }}
            />
            <CostWrapper>
              <CostText>Total</CostText>
              <Cost>$ {totalCosts}</Cost>
            </CostWrapper>
            <LeftHeader>
              <MapView>
                <InfoWrapper>
                  <div
                    style={{ marginRight: 10, fontSize: 15, display: 'flex', alignItems: 'center' }}
                  >
                    Show Map{' '}
                    <QuestionMark>
                      <QuestionIcon width={15} height={15} color="rgb(80, 80, 80)" />
                    </QuestionMark>
                    <QuestionInfo>See your day by day tour on the map.</QuestionInfo>
                  </div>{' '}
                  <SwitchSlider map />
                </InfoWrapper>
              </MapView>
              <InfoWrapper>
                <div
                  style={{ marginRight: 10, fontSize: 15, display: 'flex', alignItems: 'center' }}
                >
                  Tour Optimization{' '}
                  <QuestionMark>
                    <QuestionIcon width={15} height={15} color="rgb(80, 80, 80)" />
                  </QuestionMark>
                  <QuestionInfo left>
                    It smartly optimizies and reorders your itinerary to have least money and time
                    expenses.
                  </QuestionInfo>
                </div>{' '}
                <SwitchSlider optOn={this.optimizationOn} />
              </InfoWrapper>
            </LeftHeader>
            {lists.map(
              (list, listIndex) =>
                listIndex !== 0 && (
                  <div style={{ visibility: loading ? 'hidden' : 'visible' }}>
                    <LeftDrop
                      currentHeader={currentHeader}
                      Icon={list.icon}
                      date={list.date}
                      droppableId={list.droppableId}
                      listIndex={listIndex}
                      listId={list.Id}
                      list={list.list}
                      bookNow={list[list.length - 1] === listIndex ? 'true' : 'false'}
                    />
                  </div>
                )
            )}
            <div style={{ textAlign: 'center', marginTop: 30, fontSize: '13px', color: 'gray' }}>
              Book with agency in London
            </div>
            <Button
              style={{
                position: 'relative',
                width: 140,
                height: 40,
                margin: '-5px auto 40px',
                backgroundImage: '#7fcc0d'
              }}
            >
              Book Your Tour
            </Button>
          </LeftBar>
          <RightBar id="right-bar">
            {autocompleteClicked && (
              <DirectionsMapWrapper id="directionsMap2">
                <DirectionsMap id="directionsMap" />
                <Button left onClick={() => directionsMapClose()}>
                  Close ⭢
                </Button>
                <RoutesByDays>
                  <Day selected={!direction && true}>Nov 1</Day>
                  <Day
                    selected={direction && true}
                    onClick={() => {
                      this.setState({ direction: true })
                    }}
                  >
                    Nov 2
                  </Day>
                  <Day>Nov 3</Day>
                </RoutesByDays>
              </DirectionsMapWrapper>
            )}

            <Overlay id="right-bar-overlay" onClick={closeHandler} />

            <RightInsideWrapper>
              <RightFixedHeader />
              {this.renderHeader()}
              <RightDrop
                currentHeader={currentHeader}
                droppableId="droppable1"
                listIndex="2432"
                listId="list1"
                list={lists[0].list}
              />
            </RightInsideWrapper>
          </RightBar>
          <HotelDetailsPopup />
          <AttractionDetailsPopup />
        </DragDropContext>
      </MainWrapper>
    )
  }
}

export default TripPlanner
