import styled from 'styled-components'
import colors from '../../styles/colors'
import fonts from '../../styles/fonts'

export const LeftBar = styled.div`
  width: 50%;
  position: relative;
  @media (max-width: 768px) {
    width: 100%;
  }
  ${'' /* background-color: #f3f2f5; */} box-shadow: 0 1px 3px 0 rgba(37, 32, 31, 0.3);
  height: 100vh;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    width: 0;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 5px;
    background-color: #b5b8b8;
    outline: 1px solid slategrey;
  }
`

export const BackgroundLayer = styled.div`
  background-color: #f3f2f5;
  z-index: -3;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  min-height: 100vh;
`

export const RightBar = styled.div`
  @media (max-width: 768px) {
    display: none;
    width: 100%;
    top: 100%;
    position: absolute;
  }
  width: 50%;
  ${'' /* padding-top: 15px; */} height: 100vh;
  background-color: white;
  position: relative;
  box-shadow: 0 1px 3px 0 rgba(37, 32, 31, 0.3);
  overflow-y: scroll;
  overflow-x: hidden;
  ::-webkit-scrollbar {
    width: 5px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 5px;
    background-color: #b5b8b8;
    outline: 1px solid slategrey;
  }
`

export const QuestionInfo = styled.div`
  height: 70;
  width: 200px;
  background-color: white;
  font-size: 12px;
  visibility: hidden;
  position: absolute;
  top: 25px;
  padding: 15px;
  left: ${props => (props.left ? 0 : '87px')};
  z-index: 4;
  border-radius: 5px;
  box-shadow: 0 1px 3px 0 rgba(37, 32, 31, 0.3);
  transition-property: visibility;
  transition-duration: 0.3s;
  transition-delay: 0.1s;
`

export const QuestionMark = styled.div`
  width: 15px;
  height: 15px;
  cursor: help;
  z-index: 1;
  margin-left: 6px;
  &:hover ~ ${QuestionInfo} {
    visibility: visible;
  }
`

export const InfoWrapper = styled.div`
  display: flex;
  position: relative;
`

export const MainWrapper = styled.div`
  display: flex;
  position: relative;
`

export const BoxWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  padding: 0 10px;
  @media (max-width: 768px) {
    padding: 60px 10px;
  }
`

export const RightInsideWrapper = styled.div`
  max-width: 730px;
`

export const Button = styled.div`
  width: 80px;
  display: flex;
  justify-content: center;
  user-select: none;
  z-index: 2;
  align-items: center;
  height: 32px;
  right: ${props => props.right && '20px'};
  left: ${props => props.left && '20px'};
  line-height: 30px;
  position: absolute;

  ${'' /* right: 100%; */} top: 20px;
  color: white;
  box-shadow: 0 1px 6px 0 rgba(37, 88, 55, 0.5);
  cursor: pointer;
  border-radius: 20px;
  font-weight: ${fonts.weights.bold};
  background-image: linear-gradient(to right, #16bddf, ${colors.primaryBrand});
`

export const AddItemsWrapper = styled.div`
  width: 100%;
  height: 300px;
  z-index: 2;
  background-color: white;
  display: flex;
  padding: 15px;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.46);
  flex-direction: column;
  position: absolute;
  bottom: -100vh;
  display: none;
  @media (min-width: 768px) {
    display: none !important;
  }
`

export const AddItem = styled.div`
  padding: 0 10px 20px 10px;
  display: flex;
  align-items: center;
`

export const CategoryText = styled.div`
  margin-left: 10px;
`

export const CloseButton = styled.div`
  background-color: white;
  text-align: end;
  padding: 5px 15px 0;
  font-size: 18px;
  color: ${colors.primaryText};
`

export const Overlay = styled.div`
  position: fixed;
  width: 100%;
  display: none;
  height: 100%;
  z-index: 2;
  background-color: rgba(0, 0, 0, 0.6);
  @media (min-width: 768px) {
    display: none;
  }
`

export const LeftBarOverlay = styled.div`
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  z-index: 1;
  opacity: 0;
  background-color: rgba(0, 0, 0, 0.5);
  @media (min-width: 768px) {
    display: none;
  }
`

export const VerticalLine = styled.div`
  border: 1px dashed silver;
  height: 90%;
  width: 1px;
  top: 105px;
  left: 48px;
  z-index: -2;
  position: absolute;
`

export const DropPlaceholder = styled.div`
  max-width: 700px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 92%;
  top: 90px;
  left: 15px;
  z-index: -1;
  height: 110px;
  background-color: white;
  border: 2px dashed silver;
  border-radius: 4px;
`

export const MultipleMarkersMapWrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: absolute;
  z-index: 4;
  left: -800px;
`

export const MultipleMarkersMap = styled.div`
  width: 100%;
  height: 100%;
`

export const CostWrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  position: fixed;
  background-color: rgba(255, 255, 255, 1);
  padding: 10px 8px;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.2);
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  left: 0;
  top: 120px;
  z-index: 3;
`

export const CostText = styled.div`
  font-size: 12px;
  width: 50px;
  text-align: center;
`

export const Cost = styled.div`
  color: red;
  font-size: 19px;
  font-weight: bold;
`

export const LeftHeader = styled.div`
  margin: 20px 10px 60px auto;
  max-width: 700px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const MapView = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`

export const DirectionsMapWrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: absolute;
  z-index: 4;
  right: -800px;
`

export const DirectionsMap = styled.div`
  width: 100%;
  height: 100%;
`

export const RoutesByDays = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 70px;
  left: 20px;
`

export const Day = styled.div`
  width: 80px;
  display: flex;
  justify-content: center;
  user-select: none;
  z-index: 2;
  align-items: center;
  right: ${props => props.right && '20px'};
  left: ${props => props.left && '20px'};
  line-height: 30px;
  margin: 10px 0;
  ${'' /* position: absolute; */} ${'' /* right: 100%; */} top: 20px;
  color: white;
  box-shadow: 0 1px 6px 0 rgba(0, 0, 0, 0.5);
  cursor: pointer;
  border-radius: 20px;
  font-weight: ${fonts.weights.bold};
  background-color: ${props => (props.selected ? '#333' : '#818080')};
`
